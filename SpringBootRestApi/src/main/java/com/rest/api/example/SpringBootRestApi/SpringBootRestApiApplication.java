package com.rest.api.example.SpringBootRestApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication(scanBasePackages={"com.rest.api.example.SpringBootRestApi","com.rest.api.example.SpringBootRestApi.model","com.rest.api.example.SpringBootRestApi.controller","com.rest.api.example.SpringBootRestApi.service"})
public class SpringBootRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestApiApplication.class, args);
	}
}
